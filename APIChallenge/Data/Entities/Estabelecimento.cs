﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIChallenge
{
    public class Estabelecimento
    {
        private long id;
        private String nome;
        private String cnpj;
        private String razaoJuridica;
        private String ativa;

        public long Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Cnpj { get => cnpj; set => cnpj = value; }
        public string RazaoJuridica { get => razaoJuridica; set => razaoJuridica = value; }
        public string Ativa { get => ativa; set => ativa = value; }
    }
}
